
#include "stm32f1xx_hal.h"
#include "usart/bsp_debug_usart.h"
#include "led/bsp_led.h"
#include "pn532/bsp_usartx_PN532.h"
#include "beep/bsp_beep.h"
#include "key/bsp_key.h"


#define UART_BUFF_SIZE      1024

uint8_t data1[]={0x55,0x55,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0x03,0xFD,0xD4,0x14,0x01,0x17,0x00};  
uint8_t data2[]={0x00,0x00,0xFF,0x04,0xFC,0xD4,0x4A,0x01,0x00,0xE1,0x00};  

uint8_t aRxBuffer;
uint8_t UID[4]; 
uint8_t UID_backup[4];
__IO  uint16_t uart_p = 0;
uint8_t uart_buff[UART_BUFF_SIZE];
extern __IO uint8_t flag_nfc_status; 



void nfc_WakeUp(void);
void clean_rebuff(void);
uint8_t *get_rebuff(uint16_t *len);
void  nfc_InListPassiveTarget(void);
void nfc_read(void);
void nfc_write(uint8_t write_data);
void nfc_PsdVerifyKeyA(void);


void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE; 
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9; 
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;      
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;              
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;               
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;               
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);


  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000); 

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}


int main(void)
{
 
  HAL_Init();
 
  SystemClock_Config();


  MX_DEBUG_USART_Init();
  
  HMI_USARTx_Init();
 
  LED_GPIO_Init(); 
  BEEP_GPIO_Init();
  KEY_GPIO_Init();
  
  HAL_UART_Receive_IT(&husartx_HMI,&aRxBuffer,1); 

  nfc_WakeUp();
  printf("WakeUp Finish\n");

  while (1)
  {
    nfc_InListPassiveTarget();
    nfc_PsdVerifyKeyA();
  }
  
}

void nfc_WakeUp(void)
{
  uint8_t i;
  uint8_t temp=0;
  uint8_t CheckCode=0; 
	uint16_t len;  
  while(1)
  { 
    HAL_UART_Transmit(&husartx_HMI,&data1[0],24,0xffff);
  	while(__HAL_UART_GET_FLAG(&husartx_HMI,UART_FLAG_TXE)==0); 
    HAL_Delay(180); 
    
    get_rebuff(&len); 
 
    //00 00 FF 00 FF 00 00 00 FF 02 FE D5 15 16 00        
    if(len!=0)
    {
      for(i=11;i<13;i++)
      {
          temp+=uart_buff[i];
      }
      CheckCode=0x100-temp;
      if(CheckCode==uart_buff[13])
      {
          CheckCode=0x100-temp;
          LED1_ON;  
          LED2_ON;
          clean_rebuff();
          break;
      }       
    }
  }
}


void  nfc_InListPassiveTarget(void)
{
  uint8_t i;
  uint8_t temp=0;
  uint8_t CheckCode=0; 
	uint16_t len; 
  while(1)
  {   
    HAL_UART_Transmit(&husartx_HMI,&data2[0],11,0xffff);
  	while(__HAL_UART_GET_FLAG(&husartx_HMI,UART_FLAG_TXE)==0); 
    HAL_Delay(180); 

    get_rebuff(&len); 
    //00 00 FF 04 FC D4 4A 01 00 E1 00    
    if((len!=0)&&(uart_buff[9]!=0))
    {
  
      for(i=11;i<23;i++)
      {
          temp+=uart_buff[i];
      }
      CheckCode=0x100-temp;
      if(CheckCode==uart_buff[23])
      {
          UID[0]=uart_buff[19];
          UID[1]=uart_buff[20];
          UID[2]=uart_buff[21];
          UID[3]=uart_buff[22];  
//          if((UID[0]!=0)||(UID[1]!=0)||(UID[2]!=0)||(UID[3]!=0))
//          {
//            printf("UID为:%x %x %x %x\n",UID[0],UID[1],UID[2],UID[3]);                       
//          }
          clean_rebuff();
          break;
      }
    }
    clean_rebuff();
  }
}


void  nfc_PsdVerifyKeyA(void)
{
  uint8_t i,data[22];
  uint8_t temp=0;
  uint8_t CheckCode=0; 
	uint16_t len; 
  data[0]=0x00;
  data[1]=0x00;
  data[2]=0xFF;
  
  data[3]=0x0F; 
  data[4]=0xF1; 
  
  data[5]=0xD4; 
  data[6]=0x40; 
  
  data[7]=0x01;
  data[8]=0x60;
  data[9]=0x03; 
  
  data[10]=0xFF; //KEY A 密码 FF FF FF FF FF FF
  data[11]=0xFF;
  data[12]=0xFF;
  data[13]=0xFF;
  data[14]=0xFF;
  data[15]=0xFF;
  
  data[16]=UID[0];
  data[17]=UID[1];
  data[18]=UID[2];
  data[19]=UID[3]; 
  
  for(i=5;i<20;i++)
  {
      temp+=data[i];
  }

  data[20]=0x100-temp;   //数据 校验   0x100-  
  data[21]=0x00;  

  HAL_UART_Transmit(&husartx_HMI,&data[0],22,0xffff);//往USART2，发送 length长度的数据data
  while(__HAL_UART_GET_FLAG(&husartx_HMI,UART_FLAG_TXE)==0); //循环发送,直到发送完毕
  
  HAL_Delay(180); 
  temp=0;
  /*获取数据*/
  get_rebuff(&len); 
  //00 00 FF 04 FC D4 4A 01 00 E1 00    

  for(i=11;i<14;i++)
  {
      temp+=uart_buff[i];
  }
  CheckCode=0x100-temp;
  
  if(CheckCode==uart_buff[14])
  {
     clean_rebuff();

     if(flag_nfc_status==0)
     {      
       nfc_read();
       flag_nfc_status=0;
     }
     if(flag_nfc_status==1)
     {    
        LED1_OFF;        
        nfc_write(0x55);//修改标签为0x55
        flag_nfc_status=0;
     }     
     if(flag_nfc_status==2)
     {    
        LED2_OFF;        
        nfc_write(0xAA);//修改标签为0xAA
        flag_nfc_status=0;
     } 
  }   
}

/**
  * 函数功能: 读02区的16个字节
  * 输入参数: 无
  * 返 回 值: 无
  * 说    明: 无
  */
void nfc_read(void)
{
  uint8_t i,data[12];
  uint8_t temp=0;
  uint8_t CheckCode=0; //数据校验码
  uint16_t len; 
  data[0]=0x00;
  data[1]=0x00;
  data[2]=0xFF;
  
  data[3]=0x05; //包 长度
  data[4]=0xFB; //包 长度 校验  0x100-data[3]
  
  data[5]=0xD4; //命令标识码
  data[6]=0x40; //命令标识码
  
  data[7]=0x01;
  data[8]=0x30;
  data[9]=0x02; //读第二块的16字节数据 
  
  temp=0;
  for(i=5;i<10;i++)
  {
      temp+=data[i];
  }
  data[10]=0x100-temp; 
  data[11]=0x00;  
  HAL_UART_Transmit(&husartx_HMI,&data[0],12,0xffff);//往USART2，发送 length长度的数据data
  while(__HAL_UART_GET_FLAG(&husartx_HMI,UART_FLAG_TXE)==0); //循环发送,直到发送完毕
  
  HAL_Delay(180); 
  temp=0;
  get_rebuff(&len); 
  
  for(i=11;i<30;i++)
  {
    temp+=uart_buff[i];
  }
  CheckCode=0x100-temp;  
  if(CheckCode==uart_buff[30])
  {
    printf("读到标签为:%x\n",uart_buff[14]);
    if((UID_backup[0]!=UID[0])|(UID_backup[1]!=UID[1])|(UID_backup[2]!=UID[2])|(UID_backup[3]!=UID[3]))
    {
      BEEP_ON;//蜂鸣器 叫
      HAL_Delay(100);
      BEEP_OFF;//蜂鸣器 不叫
    }
    UID_backup[0]=UID[0];
    UID_backup[1]=UID[1];
    UID_backup[2]=UID[2];
    UID_backup[3]=UID[3];
    clean_rebuff();
  }
}


void nfc_write(uint8_t write_data)
{
  uint8_t i,data[28];
  uint8_t temp=0;
  uint8_t CheckCode=0; //数据校验码
  uint16_t len; 
  data[0]=0x00;
  data[1]=0x00;
  data[2]=0xFF;
  
  data[3]=0x15; //包 长度
  data[4]=0xEB; //包 长度 校验  0x100-data[3]
  
  data[5]=0xD4; //命令标识码
  data[6]=0x40; //命令标识码
  
  data[7]=0x01; //读写大于6字节 就置1，看手册
  data[8]=0xA0; //写
  data[9]=0x02; //写第二块的16字节数据 
  
  data[10]=write_data; //第 1 字节 数据
  data[11]=0x00;
  data[12]=0x00; //第 3 字节 数据
  data[13]=0x00;
  data[14]=0x00; //第 5 字节 数据
  data[15]=0x00;
  data[16]=0x00; //第 7 字节 数据
  data[17]=0x00;
  data[18]=0x00; //第 9 字节 数据
  data[19]=0x00;
  data[20]=0x00; //第 11 字节 数据
  data[21]=0x00;
  data[22]=0x00; //第 13 字节 数据
  data[23]=0x00;
  data[24]=0x00; //第 15 字节 数据
  data[25]=0x00;
  
  temp=0;
  for(i=5;i<26;i++)
  {
      temp+=data[i];
  }
  data[26]=0x100-temp; 
  data[27]=0x00;  
  
  HAL_UART_Transmit(&husartx_HMI,&data[0],28,0xffff);//往USART2，发送 length长度的数据data
  while(__HAL_UART_GET_FLAG(&husartx_HMI,UART_FLAG_TXE)==0); //循环发送,直到发送完毕
  
  HAL_Delay(180); 
  temp=0;
  get_rebuff(&len); 
   
  for(i=11;i<14;i++)
  {
    temp+=uart_buff[i];
  }
  CheckCode=0x100-temp;
  if(CheckCode==uart_buff[14])
  {

    if((UID_backup[0]!=UID[0])|(UID_backup[1]!=UID[1])|(UID_backup[2]!=UID[2])|(UID_backup[3]!=UID[3]))
    {
      BEEP_ON;//蜂鸣器 叫
      HAL_Delay(100);
      BEEP_OFF;//蜂鸣器 不叫
    }
    UID_backup[0]=UID[0];
    UID_backup[1]=UID[1];
    UID_backup[2]=UID[2];
    UID_backup[3]=UID[3];  
    clean_rebuff();    
  }
}

/**
  * 函数功能: 接收中断回调函数
  * 输入参数: 无
  * 返 回 值: 无
  * 说    明: 无
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{

  if(uart_p<UART_BUFF_SIZE)
  {
    uart_buff[uart_p] =aRxBuffer; 
    uart_p++; 
    HAL_UART_Receive_IT(&husartx_HMI,&aRxBuffer,1);
  }  
}

/**
  * 函数功能: 获取接收到的数据和长度 
  * 输入参数: 无
  * 返 回 值: 无
  * 说    明：无
  */
uint8_t *get_rebuff(uint16_t *len) 
{
    *len = uart_p;
    return (uint8_t *)&uart_buff;
}

/**
  * 函数功能: 清空缓冲区
  * 输入参数: 无
  * 返 回 值: 无
  * 说    明：无
  */
void clean_rebuff(void)
{
  uint16_t i=UART_BUFF_SIZE+1;
  
  uart_p = 0;
	while(i)
		uart_buff[--i]=0;
}
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
